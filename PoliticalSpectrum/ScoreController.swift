//
//  ScoreController.swift
//  PoliticalSpectrum
//
//  Created by Robert Pope on 09/05/2016.
//  Copyright © 2016 Popetech. All rights reserved.
//

import UIKit

class ScoreController: UIViewController {
    
    var questions: [Question] = []
    
    @IBOutlet weak var verticalConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var horizConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var background: UIView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
            }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.background.layoutIfNeeded()
        
        let (totalAuthoritarianScore, totalRightWingScore) = calculateTotalScore()
        
        verticalConstraint.constant = (background.frame.size.height/2) * CGFloat(totalAuthoritarianScore)
        horizConstraint.constant = (background.frame.size.width/2)  * CGFloat(totalRightWingScore)
        
        UIView.animateWithDuration(0.5) { 
            self.background.layoutIfNeeded()
        }
        
    }
    
    func calculateTotalScore() -> (authoritarian: Float, rightWing: Float) {
        
        let authoritarianQuestions = questions.filter { question in
            return question.liberal
        }
        
        let rightWingQuestions = questions.filter { question in
            return question.liberal == false
        }
        
        let totalAuthoritarianScore: Float = authoritarianQuestions.reduce(0) { (totalAuthoritarianScore, question) -> Float in
            totalAuthoritarianScore + question.answer
        }
        
        let totalRightWingScore: Float = rightWingQuestions.reduce(0) { (totalRightWingScore, question) -> Float in
            totalRightWingScore + question.answer
        }
        
        
        return (totalAuthoritarianScore / Float(authoritarianQuestions.count), totalRightWingScore / Float(rightWingQuestions.count))
    }
}
