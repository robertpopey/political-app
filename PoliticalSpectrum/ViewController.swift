//
//  ViewController.swift
//  PoliticalSpectrum
//
//  Created by Robert Pope on 22/04/2016.
//  Copyright © 2016 Popetech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var score: Float = 0.0
    var questions: [Question] = []
    var index = 0
    
    
    @IBAction func nextPage(sender: AnyObject) {
        
        if index + 2 <= questions.count - 3 {
            savesliderValues()
            incrementIndex()
            loadQuestions()
        } else {
            savesliderValues()
            performSegueWithIdentifier("ScoreSegue", sender: self)
        }
    }
    
    @IBOutlet weak var backbuttonOutlet: UIBarButtonItem!
    @IBAction func backButton(sender: AnyObject) {
        
        if index == 0 {
            savesliderValues()
            performSegueWithIdentifier("backsegue", sender: self)
        } else {
            savesliderValues()
            decrementIndex()
            loadQuestions()
        }
    }
    
    
    @IBOutlet weak var question1Label: UILabel!
    @IBOutlet weak var liberalA: UISlider!
    @IBAction func liberalsliderAChange(sender: UISlider) {
    }
    
    
    @IBOutlet weak var question2Label: UILabel!
    @IBOutlet weak var liberalB: UISlider!
    @IBAction func liberalsliderBChange(sender: UISlider) {
        
    }
    
    @IBOutlet weak var question3Label: UILabel!
    @IBOutlet weak var liberalC: UISlider!
    @IBAction func liberalsliderCChange(sender: UISlider) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        assignbackground()
        
        questions.append(Question(text: "Q1) I don't agree in capital punishment (the death penalty) for severe crimes e.g. murder", answer: 0, liberal: true))
        questions.append(Question(text: "Q2) Cannabis should be legal and regulated like alcohol or cigarettes.", answer: 0, liberal: true))
        questions.append(Question(text: "Q3) All drugs (heroin, cocaine) should be legal and regulated like alcohol or cigarettes.", answer: 0, liberal: true))
        questions.append(Question(text: "Q4) The use of guns/fire arms shouldn’t be made available to all Police Offices.", answer: 0, liberal: true))
        questions.append(Question(text: "Q5) Prostitution should be legal and regulated.", answer: 0, liberal: true))
        questions.append(Question(text: "Q6) There should be fewer CCTV cameras in public places.", answer: 0, liberal: true))
        questions.append(Question(text: "Q7) Abortion should be the choice of a woman not the state.", answer: 0, liberal: true))
        questions.append(Question(text: "Q8) Civil liberty and human rights should take priority over counter terrorism methods.", answer: 0, liberal: true))
        questions.append(Question(text: "Q9) The use of Corporal punishment in schools (teachers hitting students as punishment) does not benefit children or their education.", answer: 0, liberal: true))
        questions.append(Question(text: "Q10) I don’t consider my nationality as a key part of who I am.", answer: 0, liberal: true))
        questions.append(Question(text: "Q11) Same sexed couples should be entitled to the same rights (e.g. marriage) as everyone else.", answer: 0, liberal: true))
        questions.append(Question(text: "Q12) If regulated and between two consenting adults, I don’t object to the use and production of pornography.", answer: 0, liberal: true))
        questions.append(Question(text: "Q13) There is too much emphasis on punishing criminals instead of rehabilitating them.", answer: 0, liberal: true))
        questions.append(Question(text: "Q14) Government works at its best and most effective when there is a strong centralisation of power.", answer: 0, liberal: true))
        questions.append(Question(text: "Q15) Niccolo Machiavelli, a historical political philosopher is associated with the phrase “The end justifies the means”. Do you agree with this principle from a political context?.", answer: 0, liberal: true))
        questions.append(Question(text: "Q16) Adults who have no needs or disability should still be eligible for out of work benefits (e.g. job seekers allowance).", answer: 0, liberal: false))
        questions.append(Question(text: "Q17) I disagree in the concept of lower taxation and lower welfare.", answer: 0, liberal: false))
        questions.append(Question(text: "Q18) The government should prioritise building more social/council housing over encouraging home ownership schemes.", answer: 0, liberal: false))
        questions.append(Question(text: "Q19) Public infrastructure projects (e.g. building road/rail links) should take priority over cutting business taxes and government deregulation.", answer: 0, liberal: false))
        questions.append(Question(text: "Q20) Private/public schools are bad for society and our education system.", answer: 0, liberal: false))
        questions.append(Question(text: "Q21) Local government/States should run our schools. I disagree with academies/free schools who are independent from local government.", answer: 0, liberal: false))
        questions.append(Question(text: "Q22) University education should be fully funded through general taxation.", answer: 0, liberal: false))
        questions.append(Question(text: "Q23) Key public services such as water, electricity and the railway should be under public control – not private ownership.", answer: 0, liberal: false))
        questions.append(Question(text: "Q24) I disagree with compulsory National Service (e.g. every adult aged 16 to 21 should complete 6 months National Service).", answer: 0, liberal: false))
        questions.append(Question(text: "Q25) Private healthcare for wealthy individuals undermines public healthcare systems (e.g. NHS).", answer: 0, liberal: false))
        questions.append(Question(text: "Q26) We should increase our annual foreign aid budget.", answer: 0, liberal: false))
        questions.append(Question(text: "Q27) We should allow more genuine refugees into our country.", answer: 0, liberal: false))
        questions.append(Question(text: "Q28) I do not believe society needs religon to uphold or establish good moral values.", answer: 0, liberal: false))
        questions.append(Question(text: "Q29) I believer our country should get rid of nuclear weapons.", answer: 0, liberal: false))
        questions.append(Question(text: "Q30) A strong welfare state helps establish and maintain a healthy strong society.", answer: 0, liberal: false))
        
        loadQuestions()
        
    }
    
    func assignbackground(){
        let background = UIImage(named: "background2")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.ScaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    
    
    func currentQuestion1() -> Question {
        return questions[index]
    }
    
    func currentQuestion2() -> Question {
        return questions[index + 1]
    }
    
    func currentQuestion3() -> Question {
        return questions[index + 2]
    }
    
    func loadQuestions() {
        question1Label.text = currentQuestion1().text
        liberalA.value = currentQuestion1().answer
        
        question2Label.text = currentQuestion2().text
        liberalB.value = currentQuestion2().answer
        
        question3Label.text = currentQuestion3().text
        liberalC.value = currentQuestion3().answer
    }
    
    func savesliderValues(){
        currentQuestion1().answer = liberalA.value
        currentQuestion2().answer = liberalB.value
        currentQuestion3().answer = liberalC.value
    }
    
    func incrementIndex() {
        index = index + 3
    }
    
    func decrementIndex() {
        index = index - 3
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let scoreController = segue.destinationViewController as? ScoreController {
            scoreController.questions = questions
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}

