//
//  Infopage.swift
//  PoliticalSpectrum
//
//  Created by Robert Pope on 03/05/2016.
//  Copyright © 2016 Popetech. All rights reserved.
//

import UIKit

class InfopageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        assignbackground()
        labelDesign()
        buttonDesign()
    }
    
    func assignbackground(){
        let background = UIImage(named: "background3")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.ScaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    
    @IBOutlet weak var label: UILabel!
    
    func labelDesign() {
        
        label.backgroundColor = UIColor.whiteColor()
        label.layer.cornerRadius = 5
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.redColor().CGColor
    }

    @IBOutlet weak var btn: UIButton!
    
    func buttonDesign() {
        
        btn.backgroundColor = UIColor.whiteColor()
        btn.layer.cornerRadius = 5
        btn.layer.borderWidth = 1
        btn.layer.borderColor = UIColor.blackColor().CGColor
        
    }

    
}