//
//  StartScreen.swift
//  PoliticalSpectrum
//
//  Created by Robert Pope on 24/04/2016.
//  Copyright © 2016 Popetech. All rights reserved.
//

import UIKit

class StartScreenViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        assignbackground()
        startBorder()
    }
    
    func assignbackground(){
        let background = UIImage(named: "background")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.ScaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    
    
    
    @IBOutlet weak var start: UIButton!
    
    func startBorder() {
    
        start.backgroundColor = UIColor.whiteColor()
        start.layer.cornerRadius = 5
        start.layer.borderWidth = 1
        start.layer.borderColor = UIColor.blackColor().CGColor
        
    }
}