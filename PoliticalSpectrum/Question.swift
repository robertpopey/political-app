//
//  Question.swift
//  PoliticalSpectrum
//
//  Created by Robert Pope on 24/04/2016.
//  Copyright © 2016 Popetech. All rights reserved.
//

import Foundation

class Question {
    let text: String
    var answer: Float
    let liberal: Bool
    
    init(text: String, answer: Float, liberal: Bool){
        self.text = text
        self.answer = answer
        self.liberal = liberal
    }
}